import { GET_ARTICLE } from './actionsTypes';

export default function setArticle(article) {
	return {
		type: GET_ARTICLE,
		payload: article
	};
}
