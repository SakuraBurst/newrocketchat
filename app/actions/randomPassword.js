import { SET_OTP } from './actionsTypes';

export default function setRandomPassword(otp) {
	return {
		type: SET_OTP,
		payload: otp
	};
}
