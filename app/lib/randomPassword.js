import setRandomPassword from '../actions/randomPassword';
import store from './createStore';

function dispatch() {
	store.dispatch(
		setRandomPassword(Math.round(Math.random() * (999999 - 111111) + 111111))
	);
}

export default function OneTimePasswordFunction() {
	dispatch();
	setInterval(dispatch, 15000);
}
