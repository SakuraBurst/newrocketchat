import { SET_OTP } from '../actions/actionsTypes';

const initialState = {
	randomPassword: '123456'
};

export default function twoFactor(state = initialState, action) {
	switch (action.type) {
		case SET_OTP: {
			return { ...state, randomPassword: action.payload + '' };
		}
		default:
			return state;
	}
}
