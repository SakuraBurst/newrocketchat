export const result = [
	{
		title: 'Органы управления и контроля',
		value: [
			{
				title: 'Правление',
				value: [
					{
						title: 'Члены Правления ПАО "Россети"',
						value: [
							{
								_id: 'xfQAGXZ6CgYZAjH7t',
								username: 'colapes',
								name: 'Демин Андрей Алексанрович',
								position: 'Генеральный директор',
								status: 'online'
							},
							{
								_id: 'xfQAGXZ6CgYZAjH7t',
								username: 'colapes',
								name: 'Короктев Дмитрий Сергеевич',
								position: 'Генеральный директор',
								status: 'offline'
							},
							{
								_id: 'xfQAGXZ6CgYZAjH7t',
								username: 'colapes',
								name: 'Пятигор Александр Михайлович',
								position: 'Генеральный директор',
								status: 'offline'
							},
							{
								_id: 'xfQAGXZ6CgYZAjH7t',
								username: 'colapes',
								name: 'Сергеева Ольга Андреевна',
								position: 'Генеральный директор',
								status: 'online'
							}
						]
					}
				]
			},
			{
				title: 'Совет директоров',
				value: [
					{
						_id: 'xfQAGXZ6CgYZAjH7t',
						username: 'colapes',
						name: 'Новак Александр Валентинович',
						position: 'Генеральный директор',
						status: 'online'
					},
					{
						_id: 'xfQAGXZ6CgYZAjH7t',
						username: 'colapes',
						name: 'Аширов Станислав Олегович',
						position: 'Генеральный директор',
						status: 'online'
					},
					{
						_id: 'xfQAGXZ6CgYZAjH7t',
						username: 'colapes',
						name: 'Аюев Борис Ильич',
						position: 'Генеральный директор',
						status: 'offline'
					},
					{
						_id: 'xfQAGXZ6CgYZAjH7t',
						username: 'colapes',
						name: 'Быстров Максим Сергеевич',
						position: 'Генеральный директор',
						status: 'online'
					}
				]
			},
			{
				title: 'Ревизионная комиссия',
				value: [
					{
						_id: 'xfQAGXZ6CgYZAjH7t',
						username: 'colapes',
						name: 'Дмитриев Кирилл Александрович',
						position: 'Генеральный директор',
						status: 'offline'
					},
					{
						_id: 'xfQAGXZ6CgYZAjH7t',
						username: 'colapes',
						name: 'Дубнов Олег Маркович',
						position: 'Генеральный директор',
						status: 'offline'
					},
					{
						_id: 'xfQAGXZ6CgYZAjH7t',
						username: 'colapes',
						name: 'Калинин Александр Сергеевич',
						position: 'Генеральный директор',
						status: 'offline'
					},
					{
						_id: 'xfQAGXZ6CgYZAjH7t',
						username: 'colapes',
						name: 'Ливинский Павел Анатольевич',
						position: 'Генеральный директор',
						status: 'online'
					}
				]
			}
		]
	},
	{
		title: 'Дочерние электросетевые компании',
		value: [
			{
				_id: 'xfQAGXZ6CgYZAjH7t',
				username: 'colapes',
				name: 'Маневич Юрий Владиславович',
				position: 'Генеральный директор',
				status: 'online'
			},
			{
				_id: 'xfQAGXZ6CgYZAjH7t',
				username: 'colapes',
				name: 'Муров Андрей Евгеньевич',
				position: 'Генеральный директор',
				status: 'offline'
			},
			{
				_id: 'xfQAGXZ6CgYZAjH7t',
				username: 'colapes',
				name: 'Расстригин Михаил Алексеевич',
				position: 'Генеральный директор',
				status: 'online'
			},
			{
				_id: 'xfQAGXZ6CgYZAjH7t',
				username: 'colapes',
				name: 'Рогалев Николай Дмитриевич',
				position: 'Генеральный директор',
				status: 'offline'
			}
		]
	}
];
