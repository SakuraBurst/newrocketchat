import React from 'react';
import { Text, View } from 'react-native';
import PropTypes from 'prop-types';

import Touch from '../../utils/touch';
import Avatar from '../../containers/Avatar';
import RoomTypeIcon from '../../containers/RoomTypeIcon';
import styles, { ROW_HEIGHT } from './styles';
import { themes } from '../../constants/colors';

export { ROW_HEIGHT };

const DirectoryItemLabel = React.memo(({ text, theme, isJob }) => {
	if (!text) {
		return null;
	}
	return (
		<Text
			style={
				isJob
					? [styles.directoryItemLabel, { color: '#000000' }]
					: [styles.directoryItemLabel, { color: themes[theme].auxiliaryText }]
			}
		>
			{text}
		</Text>
	);
});

const AdressBookDirectoryItem = ({
	title,
	description,
	avatar,
	onPress,
	testID,
	style,
	baseUrl,
	user,
	rightLabel,
	type,
	theme,
	isJob
}) => (
	<Touch
		onPress={onPress}
		style={{ backgroundColor: themes[theme].backgroundColor }}
		testID={testID}
		theme={theme}
	>
		<View
			style={[styles.directoryItemContainer, styles.directoryItemButton, style]}
		>
			<View style={styles.directoryItemTextContainer}>
				<View style={styles.directoryItemTextTitle}>
					<Text
						style={[styles.directoryItemName, { color: themes[theme].titleText }]}
						numberOfLines={1}
					>
						{title}
					</Text>
				</View>
			</View>
			{rightLabel}
		</View>
	</Touch>
);

AdressBookDirectoryItem.propTypes = {
	title: PropTypes.string.isRequired,
	description: PropTypes.string,
	avatar: PropTypes.string,
	type: PropTypes.string,
	user: PropTypes.shape({
		id: PropTypes.string,
		token: PropTypes.string
	}),
	baseUrl: PropTypes.string.isRequired,
	onPress: PropTypes.func.isRequired,
	testID: PropTypes.string.isRequired,
	style: PropTypes.any,
	rightLabel: PropTypes.string,
	theme: PropTypes.string,
	isJob: PropTypes.bool
};

DirectoryItemLabel.propTypes = {
	text: PropTypes.string,
	theme: PropTypes.string
};

export default AdressBookDirectoryItem;
