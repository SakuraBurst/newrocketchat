import React from 'react';
import PropTypes from 'prop-types';
import { Text, View, Image, StatusBar } from 'react-native';
import { connect } from 'react-redux';
import { SafeAreaView, ScrollView } from 'react-navigation';
import sharedStyles from './Styles';

import { CustomHeaderButtons, Item } from '../containers/HeaderButton';
import { themes } from '../constants/colors';
import { withTheme } from '../theme';
import { themedHeader } from '../utils/navigation';
import { loginRequest as loginRequestAction } from '../actions/login';

class ArticleDetails extends React.Component {
	static navigationOptions = ({ navigation, screenProps }) => {
		return {
			...themedHeader(screenProps.theme),
			title: 'Новости',
			headerRight: (
				<CustomHeaderButtons>
					<Item iconName="star" />
				</CustomHeaderButtons>
			)
		};
	};

	static propTypes = {
		navigation: PropTypes.object,
		theme: PropTypes.string
	};

	render() {
		const { theme, currentArticle, news } = this.props;
		return (
			<SafeAreaView
				style={[
					sharedStyles.container,
					{ backgroundColor: themes[theme].backgroundColor }
				]}
			>
				<ScrollView showsVerticalScrollIndicator={false} testID="article-view-list">
					<StatusBar theme={theme} />
					<Image
						source={{ uri: news[currentArticle].pictureUrl }}
						style={{ width: '100%', height: 200 }}
					/>
					<View
						style={{
							margin: 15,
							padding: 15,
							backgroundColor: themes[theme].headerBackground
						}}
					>
						<Text
							style={{
								fontWeight: 'bold',
								color: themes[theme].titleText,
								marginBottom: 10
							}}
						>
							{news[currentArticle].title}
						</Text>
						<Text style={{ color: themes[theme].titleText }}>
							{news[currentArticle].text}
						</Text>
					</View>
				</ScrollView>
			</SafeAreaView>
		);
	}
}

const mapStateToProps = state => ({
	currentArticle: state.articlesReducer.currentArticle,
	news: state.articlesReducer.articles
});

const mapDispatchToProps = dispatch => ({
	loginRequest: params => dispatch(loginRequestAction(params))
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(withTheme(ArticleDetails));
