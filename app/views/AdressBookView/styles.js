import { StyleSheet } from 'react-native';

import sharedStyles from '../Styles';

export const ROW_HEIGHT = 54;

export default StyleSheet.create({
	directoryItemButton: {
		height: ROW_HEIGHT
	},
	directoryItemContainer: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center'
	},
	directoryItemOpen: {
		height: ROW_HEIGHT,
		width: 5,
		position: 'absolute',
		left: 0,
		zIndex: 1,
		backgroundColor: '#6FCF97'
	},
	invisabilityItem: {
		height: ROW_HEIGHT,
		width: 5
	},
	directoryItemAvatar: {
		marginRight: 12
	},
	directoryItemTextTitle: {
		flexDirection: 'row',
		alignItems: 'center'
	},
	directoryItemTextTitleUsers: {
		flexDirection: 'row',
		alignItems: 'center'
	},
	directoryItemTextContainer: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'center'
	},
	directoryItemName: {
		flex: 1,
		fontSize: 17,
		...sharedStyles.textMedium
	},
	directoryItemUsername: {
		fontSize: 17,
		...sharedStyles.textRegular
	},
	directoryItemLabel: {
		fontSize: 14,
		paddingLeft: 10,
		...sharedStyles.textRegular
	},
	safeAreaView: {
		flex: 1
	},
	list: {
		padding: 0
	},
	listContainer: {
		paddingBottom: 30
	},
	separator: {
		marginLeft: 60
	},
	toggleDropdownContainer: {
		height: 46,
		flexDirection: 'row',
		alignItems: 'center'
	},
	toggleDropdownIcon: {
		marginLeft: 20,
		marginRight: 17
	},
	toggleDropdownText: {
		flex: 1,
		fontSize: 17,
		...sharedStyles.textRegular
	},
	toggleDropdownArrow: {
		marginRight: 15
	},
	dropdownContainer: {
		width: '100%',
		position: 'absolute',
		top: 0
	},
	backdrop: {
		...StyleSheet.absoluteFill
	},
	dropdownContainerHeader: {
		height: 46,
		borderBottomWidth: StyleSheet.hairlineWidth,
		alignItems: 'center',
		flexDirection: 'row'
	},
	dropdownItemButton: {
		height: 46,
		justifyContent: 'center'
	},
	dropdownItemContainer: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center'
	},
	dropdownItemText: {
		fontSize: 18,
		flex: 1,
		...sharedStyles.textRegular
	},
	dropdownItemDescription: {
		fontSize: 14,
		flex: 1,
		marginTop: 2,
		...sharedStyles.textRegular
	},
	dropdownToggleText: {
		fontSize: 15,
		flex: 1,
		marginLeft: 15,
		...sharedStyles.textRegular
	},
	dropdownItemIcon: {
		width: 22,
		height: 22,
		marginHorizontal: 15
	},
	dropdownSeparator: {
		height: StyleSheet.hairlineWidth,
		marginHorizontal: 15,
		flex: 1
	},
	inverted: {
		transform: [{ rotate: '-90deg' }]
	},
	globalUsersContainer: {
		padding: 15
	},
	globalUsersTextContainer: {
		flex: 1,
		flexDirection: 'column'
	}
});
