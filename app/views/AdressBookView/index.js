import React from 'react';
import PropTypes from 'prop-types';
import { View, FlatList, Text } from 'react-native';
import { connect } from 'react-redux';
import { SafeAreaView } from 'react-navigation';
import _ from 'lodash';
import Touch from '../../utils/touch';
import RocketChat from '../../lib/rocketchat';
import AdressBookDirectoryItem from '../../presentation/AdressBookDirectoryItem';
import sharedStyles from '../Styles';
import I18n from '../../i18n';
import SearchBox from '../../containers/SearchBox';
import { CustomIcon } from '../../lib/Icons';
import StatusBar from '../../containers/StatusBar';
import ActivityIndicator from '../../containers/ActivityIndicator';
import {
	DrawerButton,
	CloseModalButton,
	CustomHeaderButtons,
	Item
} from '../../containers/HeaderButton';
import debounce from '../../utils/debounce';
import log from '../../utils/log';
import Options from './Options';
import { withTheme } from '../../theme';
import { themes, STATUS_COLORS } from '../../constants/colors';
import styles from './styles';
import { themedHeader } from '../../utils/navigation';
import { getUserSelector } from '../../selectors/login';
import AdressBookNest from './AdressBookNest';
import Status from '../../containers/Status';
import { result } from '../../constants/serverResponse';

class AdressBookView extends React.Component {
	static navigationOptions = ({ navigation, screenProps }) => ({
		...themedHeader(screenProps.theme),
		headerLeft: screenProps.split ? (
			<CloseModalButton navigation={navigation} testID="adressbook-view-close" />
		) : (
			<DrawerButton navigation={navigation} />
		),
		title: 'Адресная Книга',
		headerRight: (
			<CustomHeaderButtons>
				<Item iconName="star" buttonStyle={{ color: '#005C9D' }} />
			</CustomHeaderButtons>
		)
	});

	static propTypes = {
		navigation: PropTypes.object,
		baseUrl: PropTypes.string,
		isFederationEnabled: PropTypes.bool,
		user: PropTypes.shape({
			id: PropTypes.string,
			token: PropTypes.string
		}),
		theme: PropTypes.string,
		directoryDefaultView: PropTypes.string
	};

	constructor(props) {
		super(props);
		this.state = {
			data: [],
			loading: false,
			text: '',
			total: -1,
			showOptionsDropdown: false,
			globalUsers: true,
			type: 'users',
			colors: ['#005C9D', '#6FCF97', '#F2994A', '#F85C50'],
			showDropdown: new Map()
		};
	}

	componentDidMount() {
		this.load({});
	}

	onSearchChangeText = text => {
		this.setState({ text });
	};

	// eslint-disable-next-line react/sort-comp
	load = debounce(async ({ newSearch = false }, message) => {
		if (newSearch) {
			this.setState({
				data: [],
				total: -1,
				loading: false
			});
		}

		const {
			loading,
			text,
			total,
			data: { length }
		} = this.state;
		if (loading || length === total) {
			return;
		}

		this.setState({ loading: true });

		const newRegExp = new RegExp(text, 'ig');

		function find(obj) {
			const a = { ...obj };
			if (a.value[0].hasOwnProperty('_id')) {
				a.value = a.value.filter(a => newRegExp.test(a.name));
			} else {
				for (let i = 0; i < a.value.length; i++) {
					a.value[i] = find(a.value[i]);
				}
			}
			return a;
		}

		function clear(obj) {
			let a = { ...obj };
			if (a.value.length === 0) {
				return false;
			} else {
				if (a.value[0].hasOwnProperty('_id')) {
					return a;
				}
				const newArr = [];
				for (let i = 0; i < a.value.length; i++) {
					if (clear(a.value[i])) {
						newArr.push(clear(a.value[i]));
					}
				}
				a = { ...a, value: newArr };
				if (a.value.length === 0) {
					return false;
				}
				return a;
			}
		}

		function clone(arr) {
			const newArr = [];
			for (let i = 0; i < arr.length; i++) {
				if (arr[i].value[0].hasOwnProperty('_id')) {
					const a = [];
					for (let g = 0; g < arr[i].value.length; g++) {
						a.push({ ...arr[i].value[g] });
					}
					newArr.push({ ...arr[i], value: a });
				} else {
					newArr.push({ ...arr[i], value: clone(arr[i].value) });
				}
			}
			return newArr;
		}

		try {
			const { data, type, globalUsers } = this.state;
			if (text.length > 0) {
				const newArr = clone(result);
				const filtredArr = newArr.map(a => find(a));
				const fil = filtredArr.map(a => clear(a));
				const finalArr = fil.filter(a => a);
				const directories = finalArr;
				if (directories) {
					this.setState({
						data: [...data, ...directories],
						loading: false,
						total: directories.length
					});
				} else {
					this.setState({ loading: false });
				}
			} else {
				const directories = result;
				if (directories) {
					this.setState({
						data: [...data, ...directories],
						loading: false,
						total: directories.length
					});
				} else {
					this.setState({ loading: false });
				}
			}
		} catch (e) {
			console.error(e);
			this.setState({ loading: false });
		}
	}, 200);

	/*
	load = debounce(async ({ newSearch = false }, message) => {
		if (newSearch) {
			this.setState({
				data: [],
				total: -1,
				loading: false
			});
		}

		const {
			loading,
			text,
			total,
			data: { length }
		} = this.state;
		if (loading || length === total) {
			return;
		}

		this.setState({ loading: true });

		try {
			const { data, type, globalUsers } = this.state;
			const query = { text, type, workspace: globalUsers ? 'all' : 'local' };
			const directories = await RocketChat.getAdressBook({
				query,
				offset: data.length,
				count: 50,
				sort: type === 'users' ? { username: 1 } : { usersCount: -1 }
			});
			if (directories.success) {
				this.setState({
					data: [...data, ...directories.result],
					loading: false,
					total: directories.total
				});
			} else {
				this.setState({ loading: false });
			}
		} catch (e) {
			log(e);
			this.setState({ loading: false });
		}
	}, 200);
	*/

	search = () => {
		this.setState({ data: [], loading: true });
		this.load({ newSearch: true });
	};

	changeType = type => {
		this.setState({ type, data: [] }, () => this.search());
	};

	toggleWorkspace = () => {
		this.setState(
			({ globalUsers }) => ({
				globalUsers: !globalUsers,
				data: []
			}),
			() => this.search()
		);
	};

	toggleDropdown = () => {
		this.setState(({ showOptionsDropdown }) => ({
			showOptionsDropdown: !showOptionsDropdown
		}));
	};

	goRoom = async ({ rid, name, t, search }) => {
		const { navigation } = this.props;
		await navigation.navigate('RoomsListView');
		navigation.navigate('AdressBookInfoView', {
			rid,
			name,
			t,
			search
		});
	};

	onPressItem = async item => {
		const { type } = this.state;
		if (type === 'users') {
			const result = await RocketChat.createDirectMessage(item.username);
			if (result.success) {
				this.goRoom({
					rid: result.room._id,
					name: item.username,
					t: 'd',
					search: item
				});
			}
		} else {
			this.goRoom({
				rid: item._id,
				name: item.name,
				t: 'c',
				search: true
			});
		}
	};

	colorPick = () => {
		const { colors } = this.state;
		const newColor = Math.round(Math.random() * (colors.length - 1));
		const newColors = colors.filter(a => a !== colors[newColor]);
		//this.setState({ colors: newColors });
		return colors[newColor];
	};

	renderHeader = () => {
		const { type } = this.state;
		const { theme } = this.props;
		return (
			<>
				<SearchBox
					onChangeText={this.onSearchChangeText}
					onSubmitEditing={this.search}
					testID="adressbook-view-search"
				/>
			</>
		);
	};

	renderSeparator = () => {
		const { theme } = this.props;
		return (
			<View
				style={[
					sharedStyles.separator,
					styles.separator,
					{ backgroundColor: themes[theme].separatorColor }
				]}
			/>
		);
	};

	toggleAdressBookDropdown = item => {
		const { showDropdown } = this.state;
		showDropdown.set(item, !showDropdown.get(item));
		this.setState({
			showDropdown
		});
	};

	dropdownArrow = open => {
		const { theme } = this.props;

		return open ? (
			<CustomIcon
				name="arrow-down"
				size={20}
				style={[
					styles.toggleDropdownArrow,

					{ color: themes[theme].buttonBackground }
				]}
			/>
		) : (
			<CustomIcon
				name="arrow-down"
				size={20}
				style={[
					styles.toggleDropdownArrow,
					styles.inverted,
					{ color: themes[theme].buttonBackground }
				]}
			/>
		);
	};

	status = status => {
		return (
			<View
				style={[
					{
						borderRadius: 10,
						width: 10,
						height: 10,
						backgroundColor: STATUS_COLORS[status],
						borderColor: themes.light.backgroundColor
					}
				]}
			/>
		);
	};

	renderItem = ({ item, index }) => {
		const { data, type } = this.state;
		const { baseUrl, user, theme } = this.props;

		let style;
		if (index === data.length - 1) {
			style = {
				...sharedStyles.separatorBottom,
				borderColor: themes[theme].separatorColor
			};
		}
		console.log(item.status);
		const commonProps = {
			title: item.name,
			onPress: () => this.onPressItem(item),
			baseUrl,
			testID: `adressbook-view-item-${item.name}`,
			style,
			user,
			theme
		};

		if (type === 'users') {
			return (
				<AdressBookDirectoryItem
					avatar={item.username}
					description={item.username}
					rightLabel={this.status(item.status)}
					type="d"
					{...commonProps}
				/>
			);
		}
		return (
			<AdressBookDirectoryItem
				avatar={item.name}
				description={item.topic}
				rightLabel={I18n.t('N_users', { n: item.usersCount })}
				type="c"
				{...commonProps}
			/>
		);
	};

	renderNest = ({ item }) => {
		const { showDropdown, loading, colors } = this.state;
		const { baseUrl, user, theme } = this.props;
		if (item.value.length === 0) {
			return;
		}

		const render = Object.prototype.hasOwnProperty.call(item.value[0], '_id')
			? this.renderItem
			: this.renderNest;
		const key = Object.prototype.hasOwnProperty.call(item.value[0], '_id')
			? item._id
			: item.title;
		let newColor = '';
		let newPadding = 0;
		let newViewPadding = 0;
		if (!Object.prototype.hasOwnProperty.call(item.value[0], '_id')) {
			if (
				!Object.prototype.hasOwnProperty.call(item, 'newPadding') &&
				!Object.prototype.hasOwnProperty.call(item, 'newColor')
			) {
				newPadding = 10;
				newColor = '#005C9D';
				item.value.map(a => {
					a.newPadding = 20;
					a.colors = [newColor];
				});
			} else {
				newPadding = item.newPadding;
				const newColorPull = colors.filter(a => item.colors.indexOf(a) === -1);
				[newColor] = newColorPull;
				item.value.map(a => {
					a.newPadding = item.newPadding + 10;
					a.colors = [...item.colors, newColor];
				});
			}
		} else if (!Object.prototype.hasOwnProperty.call(item, 'newPadding')) {
			newPadding = 10;
			newColor = '#005C9D';
			newViewPadding = 10;
		} else {
			newPadding = item.newPadding;
			const newColorPull = colors.filter(a => item.colors.indexOf(a) === -1);
			[newColor] = newColorPull;
			newViewPadding = item.newPadding;
		}

		const commonProps = {
			onPress: () => this.toggleAdressBookDropdown(item.title),
			baseUrl,
			testID: `adressbook-view-item-${item.title}`,
			user,
			newColor,
			theme
		};

		return (
			<>
				<AdressBookNest
					type="c"
					description={item.title}
					newPadding={newPadding}
					rightLabel={this.dropdownArrow(showDropdown.get(item.title))}
					{...commonProps}
				/>
				{showDropdown.get(item.title) ? (
					<FlatList
						data={item.value}
						style={[styles.list, { paddingLeft: newViewPadding }]}
						contentContainerStyle={styles.listContainer}
						extraData={this.state}
						// keyExtractor={item => key}
						renderItem={render}
						ItemSeparatorComponent={this.renderSeparator}
						keyboardShouldPersistTaps="always"
						ListFooterComponent={loading ? <ActivityIndicator theme={theme} /> : null}
						onEndReached={() => this.load({})}
					/>
				) : null}
			</>
		);
	};

	render = () => {
		const {
			data,
			loading,
			showOptionsDropdown,
			type,
			globalUsers,
			roles
		} = this.state;
		const { isFederationEnabled, theme } = this.props;
		return (
			<SafeAreaView
				style={[
					styles.safeAreaView,
					{ backgroundColor: themes[theme].backgroundColor }
				]}
				testID="adressbook-view"
				forceInset={{ vertical: 'never' }}
			>
				<StatusBar theme={theme} />
				<FlatList
					data={data}
					style={styles.list}
					contentContainerStyle={styles.listContainer}
					extraData={this.state}
					keyExtractor={item => item.title}
					ListHeaderComponent={this.renderHeader}
					renderItem={this.renderNest}
					ItemSeparatorComponent={this.renderSeparator}
					keyboardShouldPersistTaps="always"
					ListFooterComponent={loading ? <ActivityIndicator theme={theme} /> : null}
					onEndReached={() => this.load({})}
				/>
				{showOptionsDropdown ? (
					<Options
						theme={theme}
						type={type}
						globalUsers={globalUsers}
						close={this.toggleDropdown}
						changeType={this.changeType}
						toggleWorkspace={this.toggleWorkspace}
						isFederationEnabled={isFederationEnabled}
					/>
				) : null}
			</SafeAreaView>
		);
	};
}

const mapStateToProps = state => ({
	baseUrl: state.server.server,
	user: getUserSelector(state),
	isFederationEnabled: state.settings.FEDERATION_Enabled,
	directoryDefaultView: state.settings.Accounts_Directory_DefaultView
});

export default connect(mapStateToProps)(withTheme(AdressBookView));
