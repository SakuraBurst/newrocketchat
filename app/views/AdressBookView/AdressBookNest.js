import React from 'react';
import { Text, View } from 'react-native';
import PropTypes from 'prop-types';

import Touch from '../../utils/touch';
import Avatar from '../../containers/Avatar';
import RoomTypeIcon from '../../containers/RoomTypeIcon';
import styles from './styles';
import { themes } from '../../constants/colors';

const DirectoryItemLabel = React.memo(({ text, theme }) => {
	if (!text) {
		return null;
	}
	return (
		<Text
			style={[styles.directoryItemLabel, { color: themes[theme].auxiliaryText }]}
		>
			{text}
		</Text>
	);
});

const AdressBookNest = ({
	title,
	description,
	onPress,
	testID,
	style,
	rightLabel,
	type,
	newPadding,
	newColor,
	open,
	theme
}) => {
	return (
		<Touch
			onPress={onPress}
			style={{ backgroundColor: themes[theme].backgroundColor }}
			testID={testID}
			theme={theme}
		>
			<View
				style={[styles.directoryItemContainer, styles.directoryItemButton, style]}
			>
				<View style={styles.directoryItemTextContainer}>
					<View style={styles.directoryItemTextTitleUsers}>
						<View style={[styles.directoryItemOpen, { backgroundColor: newColor }]} />
						<Text
							style={[
								styles.directoryItemUsername,
								{ color: themes[theme].titleText, paddingLeft: newPadding }
							]}
							numberOfLines={1}
						>
							{description}
						</Text>
					</View>
				</View>
				{rightLabel}
			</View>
		</Touch>
	);
};

AdressBookNest.propTypes = {
	title: PropTypes.string.isRequired,
	description: PropTypes.string,
	newColor: PropTypes.string,
	newPadding: PropTypes.number,
	type: PropTypes.string,
	user: PropTypes.shape({
		id: PropTypes.string,
		token: PropTypes.string
	}),
	baseUrl: PropTypes.string.isRequired,
	onPress: PropTypes.func.isRequired,
	testID: PropTypes.string.isRequired,
	style: PropTypes.any,
	rightLabel: PropTypes.string,
	theme: PropTypes.string
};

DirectoryItemLabel.propTypes = {
	text: PropTypes.string,
	theme: PropTypes.string
};

export default AdressBookNest;
