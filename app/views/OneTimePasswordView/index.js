import React from 'react';
import { View, Linking, ScrollView, Text, Share } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { SafeAreaView } from 'react-navigation';

import { logout as logoutAction } from '../../actions/login';
import { selectServerRequest as selectServerRequestAction } from '../../actions/server';
import { toggleCrashReport as toggleCrashReportAction } from '../../actions/crashReport';
import { themes } from '../../constants/colors';
import { DrawerButton, CloseModalButton } from '../../containers/HeaderButton';
import StatusBar from '../../containers/StatusBar';
import { DisclosureImage } from '../../containers/DisclosureIndicator';
import I18n from '../../i18n';
import {
	getReadableVersion,
	getDeviceModel,
	isAndroid
} from '../../utils/deviceInfo';
import scrollPersistTaps from '../../utils/scrollPersistTaps';
import { showErrorAlert } from '../../utils/info';
import styles from './styles';
import sharedStyles from '../Styles';
import { withTheme } from '../../theme';
import { themedHeader } from '../../utils/navigation';
import { withSplit } from '../../split';
import { appStart as appStartAction } from '../../actions';
import { getUserSelector } from '../../selectors/login';
import setArticle from '../../actions/articles';

const SectionSeparator = React.memo(({ theme }) => (
	<View
		style={[
			styles.sectionSeparatorBorder,
			{
				borderColor: themes[theme].separatorColor,
				backgroundColor: themes[theme].auxiliaryBackground
			}
		]}
	/>
));
SectionSeparator.propTypes = {
	theme: PropTypes.string
};

const ItemInfo = React.memo(({ info, theme }) => (
	<View
		style={[
			styles.infoContainer,
			{ backgroundColor: themes[theme].auxiliaryBackground }
		]}
	>
		<Text style={[styles.infoText, { color: themes[theme].infoText }]}>
			{info}
		</Text>
	</View>
));
ItemInfo.propTypes = {
	info: PropTypes.string,
	theme: PropTypes.string
};

class OneTimePasswordView extends React.Component {
	static navigationOptions = ({ navigation, screenProps }) => ({
		...themedHeader(screenProps.theme),
		headerLeft: screenProps.split ? (
			<CloseModalButton
				navigation={navigation}
				testID="onetimepassword-view-close"
			/>
		) : (
			<DrawerButton navigation={navigation} />
		),
		title: 'One Time Password'
	});

	static propTypes = {
		navigation: PropTypes.object,
		server: PropTypes.object,
		allowCrashReport: PropTypes.bool,
		toggleCrashReport: PropTypes.func,
		theme: PropTypes.string,
		split: PropTypes.bool,
		logout: PropTypes.func.isRequired,
		selectServerRequest: PropTypes.func,
		token: PropTypes.string,
		appStart: PropTypes.func
	};

	constructor(props) {
		super(props);
		this.state = {
			email: ''
		};
	}

	componentDidMount() {
		const { userInfo } = this.props;
		this.setState({ email: userInfo.emails[0].address });
	}

	navigateToScreen = (screen, id) => {
		const { navigation, currentArticle } = this.props;
		navigation.navigate(screen);
		currentArticle(id);
	};

	sendEmail = async () => {
		const subject = encodeURI('React Native App Support');
		const email = encodeURI('support@rocket.chat');
		const description = encodeURI(`
			version: ${getReadableVersion}
			device: ${getDeviceModel}
		`);
		try {
			await Linking.openURL(
				`mailto:${email}?subject=${subject}&body=${description}`
			);
		} catch (e) {
			showErrorAlert(
				I18n.t('error-email-send-failed', { message: 'support@rocket.chat' })
			);
		}
	};

	shareApp = () => {
		Share.share({ message: isAndroid ? PLAY_MARKET_LINK : APP_STORE_LINK });
	};

	renderDisclosure = () => {
		const { theme } = this.props;
		return <DisclosureImage theme={theme} />;
	};

	render() {
		const { split, theme, userInfo, otp } = this.props;
		const { email } = this.state;
		const otpWithDefis = otp.replace(/^([\d]{3})([\d]{3})$/, '$1-$2');
		const testKeys = Object.keys(userInfo).map((a, i) => (
			<Text key={i} style={styles.thickText}>
				{a}
			</Text>
		));
		return (
			<SafeAreaView
				style={[
					sharedStyles.container,
					{ backgroundColor: themes[theme].backgroundColor }
				]}
				testID="onetimepassword-view"
				forceInset={{ vertical: 'never' }}
			>
				<StatusBar theme={theme} />
				<ScrollView
					{...scrollPersistTaps}
					contentContainerStyle={styles.listPadding}
					showsVerticalScrollIndicator={false}
					testID="onetimepassword-view-list"
				>
					<View
						style={[
							styles.infoContainer,
							{ backgroundColor: themes[theme].headerBackground }
						]}
					>
						<Text style={styles.otpText}>{otpWithDefis}</Text>
						<Text style={{ color: themes[theme].titleText }}>{email}</Text>
					</View>
				</ScrollView>
			</SafeAreaView>
		);
	}
}

const mapStateToProps = state => ({
	otp: state.twoFactor.randomPassword,
	userInfo: getUserSelector(state),
	allowCrashReport: state.crashReport.allowCrashReport,
	news: state.articlesReducer.articles
});

const mapDispatchToProps = dispatch => ({
	logout: () => dispatch(logoutAction()),
	selectServerRequest: params => dispatch(selectServerRequestAction(params)),
	toggleCrashReport: params => dispatch(toggleCrashReportAction(params)),
	appStart: (...params) => dispatch(appStartAction(...params)),
	currentArticle: article => dispatch(setArticle(article))
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(withTheme(withSplit(OneTimePasswordView)));
