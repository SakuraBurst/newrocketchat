import React from 'react';
import { View, Text, ActivityIndicator } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import I18n from '../../i18n';
import Button from '../../containers/Button';
import styles from './styles';
import { themes } from '../../constants/colors';
import { withTheme } from '../../theme';
import FormContainer, {
	FormContainerInner
} from '../../containers/FormContainer';
import ServerAvatar from './ServerAvatar';
import { getShowLoginButton } from '../../selectors/login';
import { serverRequest } from '../../actions/server';

class WorkspaceView extends React.Component {
	static navigationOptions = () => ({
		header: null
	});

	static propTypes = {
		navigation: PropTypes.object,
		theme: PropTypes.string,
		Site_Name: PropTypes.string,
		Site_Url: PropTypes.string,
		server: PropTypes.string,
		Assets_favicon_512: PropTypes.object,
		registrationEnabled: PropTypes.bool,
		registrationText: PropTypes.string,
		showLoginButton: PropTypes.bool
	};

	constructor(props) {
		super(props);
		this.state = {
			loading: true
		};
	}

	componentDidMount() {
		this.props.appStart('https://chat.digital-superhero.ru/');
	}

	UNSAFE_componentWillReceiveProps() {
		this.setState({
			loading: false
		});
	}

	login = () => {
		const { navigation, Site_Name } = this.props;
		navigation.navigate('LoginView', { title: Site_Name });
	};

	reconnect() {
		this.props.appStart('https://chat.digital-superhero.ru/');
	}

	register = () => {
		const { navigation, Site_Name } = this.props;
		navigation.navigate('RegisterView', { title: Site_Name });
	};

	render() {
		const {
			theme,
			Site_Name,
			Site_Url,
			Assets_favicon_512,
			server,
			registrationEnabled,
			registrationText,
			showLoginButton
		} = this.props;
		return (
			<FormContainer theme={theme}>
				{!this.state.loading ? (
					<FormContainerInner>
						<View style={styles.alignItemsCenter}>
							<ServerAvatar
								theme={theme}
								url={server}
								image={Assets_favicon_512 && Assets_favicon_512.defaultUrl}
							/>
							<Text style={[styles.serverName, { color: themes[theme].titleText }]}>
								{'Добро пожаловать'}
							</Text>
							<Text style={[styles.serverUrl, { color: themes[theme].auxiliaryText }]}>
								{'в корпоративное приложение ПАО "РОССЕТИ"'}
							</Text>
						</View>
						{showLoginButton ? (
							<Button
								title="Войти"
								type="primary"
								onPress={this.login}
								theme={theme}
							/>
						) : (
							<Button
								title="Обновить"
								type="primary"
								onPress={this.reconnect}
								theme={theme}
							/>
						)}
						{registrationEnabled ? (
							<Button
								title="Создать аккаунт"
								type="secondary"
								backgroundColor={themes[theme].chatComponentBackground}
								onPress={this.register}
								theme={theme}
							/>
						) : (
							<Text
								style={[
									styles.registrationText,
									{ color: themes[theme].auxiliaryText }
								]}
							>
								{registrationText}
							</Text>
						)}
					</FormContainerInner>
				) : (
					<FormContainerInner>
						<View style={styles.justifyContentsCenter}>
							<ActivityIndicator size="large" color="#005C9D" />
						</View>
					</FormContainerInner>
				)}
			</FormContainer>
		);
	}
}

const mapStateToProps = state => ({
	server: state.server.server,
	adding: state.server.adding,
	Site_Name: state.settings.Site_Name,
	Site_Url: state.settings.Site_Url,
	Assets_favicon_512: state.settings.Assets_favicon_512,
	registrationEnabled: state.settings.Accounts_RegistrationForm === 'Public',
	registrationText: state.settings.Accounts_RegistrationForm_LinkReplacementText,
	showLoginButton: getShowLoginButton(state)
});

const mapDispatchToProps = dispatch => ({
	appStart: request => dispatch(serverRequest(request))
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(withTheme(WorkspaceView));
