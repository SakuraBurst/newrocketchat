import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
	statsHead: {
		paddingTop: 10,
		paddingHorizontal: 12
	},
	container: {
		width: '100%',
		marginTop: 10
	},
	scrollView: {
		display: 'flex',
		flexDirection: 'row',
		overflow: 'hidden',
		justifyContent: 'space-between'
	},
	bullets: {
		position: 'absolute',
		top: 0,
		right: 0,
		display: 'flex',
		justifyContent: 'flex-start',
		flexDirection: 'row',
		paddingHorizontal: 10,
		paddingTop: 5
	},
	bullet: {
		paddingHorizontal: 5,
		fontSize: 20
	}
});

export default styles;
