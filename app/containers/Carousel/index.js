import React from 'react';
import { View, ScrollView, Text } from 'react-native';
import { Slide } from './CarouselItem';
import { styles } from './styles';

export const Carousel = (props) => {
	const { items, url } = props;
	const itemsPerInterval =
		props.itemsPerInterval === undefined ? 1 : props.itemsPerInterval;

	const [intervals, setIntervals] = React.useState(1);
	const [width, setWidth] = React.useState(0);

	const init = (width) => {
		// initialise width
		setWidth(width);
		// initialise total intervals
		const totalItems = items.length;
		setIntervals(Math.ceil(totalItems / itemsPerInterval));
	};

	/*const bullets = [];
	for (let i = 1; i <= intervals; i++) {
		bullets.push(
			<Text
				key={i}
				style={{
					...styles.bullet,
					opacity: interval === i ? 0.5 : 0.1
				}}
			>
				&bull;
			</Text>
		);
	}*/

	return (
		<View style={styles.container}>
			<ScrollView
				horizontal={true}
				contentContainerStyle={{
					...styles.scrollView,
					width: `${100 * intervals}%`
				}}
				showsHorizontalScrollIndicator={false}
				onContentSizeChange={(w, h) => init(w)}
				onScroll={(data) => {
					setWidth(data.nativeEvent.contentSize.width);
				}}
				scrollEventThrottle={200}
				decelerationRate="fast"
				pagingEnabled
			>
				{items.map((item, index) => {
					return (
						<Slide
							key={index}
							id={item.id}
							title={item.title}
							picture={item.pictureUrl}
							last={items.length - 1 === index ? true : false}
						/>
					);
				})}
			</ScrollView>
		</View>
	);
};

export default Carousel;
